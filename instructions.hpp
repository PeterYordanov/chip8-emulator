#ifndef INSTRUCTIONS_HPP
#define INSTRUCTIONS_HPP

#define CLS
#define RET
#define SYS
#define JP
#define CALL
#define SE
#define SNE
#define SE_
#define LD
#define ADD
#define LD_
#define OR
#define AND
#define XOR
#define ADD_
#define SUB
#define SHR
#define SUBN
#define SHL
#define SNE_
#define LD_1
#define JP_
#define RND
#define DRW
#define SKP
#define SKNP
#define LD_2
#define LD_3
#define LD_4
#define LD_5
#define ADD_1
#define LD_6
#define LD_7
#define LD_8
#define LD_9

/*
 * Chip-8 Instructions
 */

//Standard
void _0nnn_();
void _00E0_();
void _00EE_();
void _1nnn_();
void _2nnn_();
void _3xkk_();
void _4xkk_();
void _5xy0_();
void _6xkk_();
void _7xkk_();
void _8xy0_();
void _8xy1_();
void _8xy2_();
void _8xy3_();
void _8xy4_();
void _8xy5_();
void _8xy6_();
void _8xy7_();
void _8xyE_();
void _9xy0_();
void _Annn_();
void _Bnnn_();
void _Cxkk_();
void _Dxyn_();
void _Ex9E_();
void _ExA1_();
void _Fx07_();
void _Fx0A_();
void _Fx15_();
void _Fx18_();
void _Fx1E_();
void _Fx29_();
void _Fx33_();
void _Fx55_();
void _Fx65_();

//Super Chip-48 Instructions
void _00Cn_();
void _00FB_();
void _00FC_();
void _00FD_();
void _00FE_();
void _00FF_();
void _Dxy0_();
void _Fx30_();
void _Fx75_();
void _Fx85_();

#endif // INSTRUCTIONS_HPP
