#include <QApplication>
#include "chip8.hpp"
#include <QFileInfo>
#include <QDebug>
#include <QDateTime>
#include <QFileDialog>

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	QString ch8file = QFileDialog::getOpenFileName();

	QFileInfo finfo(ch8file);
	if(finfo.suffix() != QString("ch8"))
		return 1;

	chip8 chip;
	chip.init();
	chip.load_binary(ch8file.toStdString().c_str());
	if(chip.is_opened()) {
		qDebug() << "File path : " << finfo.absoluteFilePath() << "\n"
		<< "File name : " << finfo.fileName() << "\n"
		<< "Size (bytes) : " << finfo.size() << "\n"
		<< "Owner : " << finfo.owner() << "\n"
		<< "Owner ID : " << finfo.ownerId() << "\n"
		<< "Last read : " << finfo.lastRead() << "\n"
		<< "Last modified : " << finfo.lastModified() << "\n";

		chip.run();
	}

	return app.exec();
}
