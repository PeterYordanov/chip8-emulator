#ifndef CHIP8_HPP
#define CHIP8_HPP

#include "keys.hpp"
#include <SDL/SDL.h>

class chip8
{
public:
	chip8();
	void init();
	void load_binary(const char*);
	void run();

private:
	void _poll_events_();
	void _draw_on_screen_();
	__attribute__((always_inline)) void _print_instr_and_register_states_()
	{
		std::printf("op: %X\n", op);
		std::printf("%d\n", delay_timer);
		std::printf("& op: %X\n", op & 0xF000);
		std::printf("&& op: %X\n", op & 0x00FF);
	}

public:
	__attribute__((always_inline)) bool is_opened()
	{
		return _is_opened;
	}

private:
	SDL_Surface* screen = nullptr;
	bool draw = false;
	bool has_quit = false;
	bool _is_opened;
	std::uint8_t memory[CHIP8_MAX_RAM];
	std::uint8_t keyboard[CHIP8_MAX_KEYBOARD_KEYS];
	std::uint8_t vga[CHIP8_MAX_WIDTH][CHIP8_MAX_HEIGHT];
	std::uint8_t oldvga[CHIP8_MAX_WIDTH][CHIP8_MAX_HEIGHT];
	std::uint8_t V[CHIP8_MAX_INSTR];
	std::uint16_t stack[CHIP8_MAX_STACK_SIZE];
	std::uint16_t I; //Index register
	std::uint16_t pc;
	std::uint16_t sp;
	std::uint8_t delay_timer;
	std::uint8_t sound_timer;
	std::uint16_t op;
};

#endif // CHIP8_HPP
