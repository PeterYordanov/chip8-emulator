#include "chip8.hpp"
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <random>

chip8::chip8(){}

void chip8::init()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	screen = SDL_SetVideoMode(64 * 10, 32 * 10, 32, 0);

	has_quit = false;
	draw = false;
	I = 0x00;
	sp = 0x00;
	pc = 0x200;
	delay_timer = 0x00;
	sound_timer = 0x00;
	op = 0x00;

	std::memset(&memory, 0, CHIP8_MAX_RAM);
	std::memset(&keyboard, 0, CHIP8_MAX_KEYBOARD_KEYS);
	std::memset(&V, 0, CHIP8_MAX_INSTR);
	std::memset(&stack, 0, CHIP8_MAX_STACK_SIZE * sizeof(std::uint16_t));

	for(std::uint8_t i = 1; i < CHIP8_MAX_FONTSET; i++)
		memory[i] = fontset[i];

	for(int i = 0; i < CHIP8_MAX_WIDTH; ++i)
		for(int j = 0; j < CHIP8_MAX_HEIGHT; ++j)
			vga[i][j] = 0;
}

void chip8::load_binary(const char* file)
{
	std::printf("Loading file: %s\n", file);

	std::ifstream _file(file, std::ios::binary | std::ios::in);
	if (!_file.fail()) {
		_file.seekg(0, _file.end);
		std::fpos<__mbstate_t> size = _file.tellg();
		//file.cur;
		_file.seekg(0, _file.beg);
		_file.read(reinterpret_cast<char*>(&memory[pc]), size);
	} else
		std::printf("Couldn't open file %s", file);

	_file.close();
	_is_opened = true;

	SDL_WM_SetCaption(file, nullptr);
}

void chip8::_poll_events_()
{
	SDL_Event event;
	while (SDL_PollEvent (&event)) {
		if (event.type == SDL_KEYDOWN)
			switch(event.key.keysym.sym) {
				case SDLK_1: keyboard[KEY_1] = 1; break;
				case SDLK_2: keyboard[KEY_2] = 1; break;
				case SDLK_3: keyboard[KEY_3] = 1; break;
				case SDLK_4: keyboard[KEY_4] = 1; break;
				case SDLK_q: keyboard[KEY_Q] = 1; break;
				case SDLK_w: keyboard[KEY_W] = 1; break;
				case SDLK_e: keyboard[KEY_E] = 1; break;
				case SDLK_r: keyboard[KEY_R] = 1; break;
				case SDLK_a: keyboard[KEY_A] = 1; break;
				case SDLK_s: keyboard[KEY_S] = 1; break;
				case SDLK_d: keyboard[KEY_D] = 1; break;
				case SDLK_f: keyboard[KEY_F] = 1; break;
				case SDLK_z: keyboard[KEY_Z] = 1; break;
				case SDLK_x: keyboard[KEY_X] = 1; break;
				case SDLK_c: keyboard[KEY_C] = 1; break;
				case SDLK_v: keyboard[KEY_V] = 1; break;
				case SDLK_ESCAPE: exit(1);
				default: break; //Fuck off, warning
			}
		else if (event.type == SDL_KEYUP)
			switch(event.key.keysym.sym) {
				case SDLK_1: keyboard[KEY_1] = 0; break;
				case SDLK_2: keyboard[KEY_2] = 0; break;
				case SDLK_3: keyboard[KEY_3] = 0; break;
				case SDLK_4: keyboard[KEY_4] = 0; break;
				case SDLK_q: keyboard[KEY_Q] = 0; break;
				case SDLK_w: keyboard[KEY_W] = 0; break;
				case SDLK_e: keyboard[KEY_E] = 0; break;
				case SDLK_r: keyboard[KEY_R] = 0; break;
				case SDLK_a: keyboard[KEY_A] = 0; break;
				case SDLK_s: keyboard[KEY_S] = 0; break;
				case SDLK_d: keyboard[KEY_D] = 0; break;
				case SDLK_f: keyboard[KEY_F] = 0; break;
				case SDLK_z: keyboard[KEY_Z] = 0; break;
				case SDLK_x: keyboard[KEY_X] = 0; break;
				case SDLK_c: keyboard[KEY_C] = 0; break;
				case SDLK_v: keyboard[KEY_V] = 0; break;
				default: break; //heheheheheh
			}
		else if (event.type == SDL_QUIT) has_quit = true;
	}
}

void chip8::_draw_on_screen_()
{
	SDL_Rect rect;
	rect.x = 0;
	rect.y = 0;
	rect.h = 64 * 10;
	rect.w = 64 * 10;

	for (short int i = 0; i < 64; i++)
		for (short int j = 0; j < 32; j++)
			if ((vga[i][j] ^ oldvga[i][j]) == 1) {
				rect.x = i * 10;
				rect.y = j * 10;
				rect.w =  10;
				rect.h =  10;
				if (vga[i][j] == 1)
					SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 255, 255, 255));

				if (vga[i][j] == 0)
					SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 0, 0, 0));
			}

	SDL_Flip(screen);
	draw = false;
}

void chip8::run()
{
	auto tick = [this]() {
		if(delay_timer > 0) {
			delay_timer--;
		}

		if(sound_timer > 0) {
			sound_timer--;
		}
	};

	while(!has_quit) {
		op = memory[pc + 0] << 8 | memory[pc + 1];

		_poll_events_();

		if(draw) _draw_on_screen_();

		switch(op & 0xF000) {
			case 0x0000: {
				switch(op & 0x00FF) {
					case 0x00E0:
						for(int i=0; i < 64; ++i)
							for(int j=0; j < 32; ++j)
								vga[i][j] = 0;

						draw = true;
						pc += 2;

					break;

					case 0X00EE:
						sp--;
						pc = stack[sp];
						pc += 2;
					break;

					default:
							unknown_instr();
					break;
				}
			}
			break;

			case 0x1000:
				pc = op & 0X0FFF;
			break;

			case 0x2000:
				stack[sp] = pc;
				sp++;
				pc = op & 0x0FFF;
			break;

			case 0x3000:
				pc += V[(op & 0x0F00) >> 8] == (op & 0x00FF) ? 4 : 2;
			break;

			case 0x4000:
				pc += V[(op & 0x0F00) >> 8] != ((op & 0x00FF)) ? 4 : 2;
			break;

			case 0x5000:
				pc += V[(op & 0x0F00) >> 8] == V[(op & 0x00F0) >> 4] ? 4 : 2;
			break;

			case 0x6000:
				V[(op & 0x0F00) >> 8] = op & 0x00FF;
				pc += 2;
			break;

			case 0x7000:
				V[(op & 0x0F00) >> 8] += op & 0x00FF;
				pc += 2;
			break;

			case 0x8000: {
				switch(op & 0x000F) {
					case 0x0000:
						V[(op & 0x0F00) >> 8] = V[(op & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0001:
						V[(op & 0x0F00) >> 8] |= V[(op & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0002:
						V[(op & 0x0F00) >> 8] &= V[(op & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0003:
						V[(op & 0x0F00) >> 8] ^= V[(op & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0004:
						V[0xF] = V[(op & 0x00F0) >> 4] > (0xFF - V[(op & 0x0F00) >> 8]) ? 0x01 : 0x00;
						V[(op & 0x0F00) >> 8] += V[(op & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0005:
						V[0xF] = V[(op & 0x00F0) >> 4] > V[(op & 0x0F00) >> 8] ? 0x00 : 0x01;
						V[(op & 0x0F00) >> 8] -= V[(op & 0x00F0) >> 4];
						pc += 2;
					break;

					case 0x0006:
						V[0xF] = V[(op & 0x0F00) >> 8] & 0x1;
						V[(op & 0x0F00) >> 8] >>= 1;
						pc += 2;
					break;

					case 0x0007:
						V[0xF] = V[(op & 0x0F00) >> 8] > V[(op & 0x00F0) >> 4] ? 0x00 : 0x01;
						V[(op & 0x0F00) >> 8] = V[(op & 0x00F0) >> 4] - V[(op & 0x0F00) >> 8];
						pc += 2;
					break;

					case 0x000E:
						V[0xF] = V[(op & 0x0F00) >> 8] >> 7;
						V[(op & 0x0F00) >> 8] <<= 1;
						pc += 2;
					break;

					default:
						unknown_instr();
					break;
				}
			}
			break;

			case 0x9000:
				pc += V[(op & 0x0F00) >> 8] != V[(op & 0x00F0) >> 4] ? 4 : 2;
			break;

			case 0xA000:
				I = op & 0x0FFF;
				pc += 2;
			break;

			case 0xB000:
				pc = (op & 0x0FFF) + V[0x0];
			break;

			case 0xC000: {
				std::mt19937 generator;
				generator.seed(std::random_device()());
				std::uniform_int_distribution<std::mt19937::result_type> random(0);
				V[(op & 0x0F00) >> 8] = (random(generator) % 0xFF) & (op & 0x00FF);
				pc += 2;
			}
			break;

			case 0xD000: {
				V[0xF] = 0x00;

				for (int i = 0; i < CHIP8_MAX_WIDTH; i++)
					for (int j = 0; j < CHIP8_MAX_HEIGHT; j++)
						oldvga[i][j] = vga[i][j];

				for(int i = 0; i < (op & 0x000F); i++) {
					std::uint8_t pixel = memory[I + i];
					for(int j=0; j < 8; j++) {
						if((pixel & (0x80 >> j)) != 0) {

							if(vga[V[(op & 0x0F00) >> 8] + j][V[(op & 0x00F0) >> 4] + i] == 1)
								V[0xF] = 0x01;

							vga[V[(op & 0x0F00) >> 8] + j][V[(op & 0x00F0) >> 4] + i] ^= 1;
						}
					}
				}

				draw = true;
				pc += 2;
			}
			break;

			case 0xE000: {
				switch(op & 0x00FF) {
					case 0x009E:
						if (keyboard[V[(op & 0x0F00) >> 8]] == 1) {
							keyboard[V[(op & 0x0F00) >> 8]] = 0;
							pc += 4;
						} else {
							pc += 2;
						}

					break;

					case 0x00A1 :
						pc += keyboard[V[(op & 0x0F00) >> 8]] != 1 ? 4 : 2;
					break;

					default:
						unknown_instr();
					break;
				}
			}
			break;


			case 0xF000: {
				switch(op & 0x00FF) {
					case 0x0007:
						V[(op & 0x0F00) >> 8] = delay_timer;
						pc += 2;
					break;

					case 0x000A: {
						bool is_key_pressed = false;

						for (int i=0; i < 16; ++i) {
							if (keyboard[i] != 0) {
								keyboard[i] = 0;
								V[(op & 0x0F00) >> 8] = i;
								is_key_pressed = true;
							}
						}

						if (is_key_pressed) {
							break;
						}

						pc += 2;
					}
					break;

					case 0x0015:
						delay_timer = V[(op & 0x0F00) >> 8];
						pc += 2;
					break;

					case 0x0018:
						sound_timer = V[(op & 0x0F00) >> 8];
						pc += 2;
					break;

					case 0x001E:
						if(I + V[(op & 0x0F00) >> 8] > 0xFFF)
							V[0xF] = 0x01;
						else
							V[0xF] = 0x00;

						I += V[(op & 0x0F00) >> 8];
						pc += 2;
					break;

					case 0x0029:
						I = V[(op & 0x0F00) >> 8] * 0x5;
						pc += 2;
					break;

					case 0x0033:
						memory[I] = V[(op & 0x0F00) >> 8] / 100;
						memory[I + 1] = (V[(op & 0x0F00) >> 8] / 10) % 10;
						memory[I + 2] = (V[(op & 0x0F00) >> 8] % 100) % 10;
						pc += 2;
					break;

					case 0x0055:
						for (int i=0; i<=((op & 0x0F00) >> 8); ++i)
							memory[I + i] = V[i];

						I += ((op & 0x0F00) >> 8) + 1;
						pc += 2;
					break;

					case 0x0065:
						for (int i=0; i<=((op & 0x0F00) >> 8); ++i)
							V[i] = memory[I + i];

						I += ((op & 0x0F00) >> 8) + 1;
						pc += 2;
					break;

					default:
						unknown_instr();
					break;
				}
			}
			break;

			default:
				unknown_instr();
			break;
		}

		tick();

		SDL_Delay(1);

	}
}
